package com.example.common_rabbitmq;

import com.example.common_rabbitmq.bean.Book;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class CommonRabbitMqApplicationTests {
    @Autowired
    RabbitTemplate rabbitTemplate;
    @Test
    void contextLoads() {
    Map map = new HashMap<>();
    map.put("msg","第一个MQ消息");
    map.put("data", Arrays.asList("王",18,true));
                   //发送           //交换器                    //匹配队列的条件        //发送的消息
    rabbitTemplate.convertAndSend("Exchanges.fanout","itguigu.*",new Book("红楼梦","曹雪芹"));
    }
@Test
    void recevice(){        //接收消息                   //根据队列返回消息
    Object o = rabbitTemplate.receiveAndConvert("itguigu.news");
    System.out.println(o.getClass());
    System.out.println(o);
}
}
