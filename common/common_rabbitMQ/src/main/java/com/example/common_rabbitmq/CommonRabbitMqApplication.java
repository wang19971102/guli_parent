package com.example.common_rabbitmq;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@EnableRabbit
@SpringBootApplication
@ComponentScan(basePackages = {"com.example"})
public class CommonRabbitMqApplication {

    public static void main(String[] args) {
        SpringApplication.run(CommonRabbitMqApplication.class, args);
    }

}
