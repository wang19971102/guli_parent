package com.example.common_rabbitmq.listener;

import com.example.common_rabbitmq.bean.Book;
import org.springframework.stereotype.Service;

@Service
public class RabbitListener {
    @org.springframework.amqp.rabbit.annotation.RabbitListener(queues = "itguigu.book")
    public void receive(Book book){
        System.out.println("收到消息："+book);
    }
}
