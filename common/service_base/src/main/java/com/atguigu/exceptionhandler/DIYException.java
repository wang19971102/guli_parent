package com.atguigu.exceptionhandler;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DIYException extends  RuntimeException{
    private Integer codeDIY;
    private String msgDIY;
}
