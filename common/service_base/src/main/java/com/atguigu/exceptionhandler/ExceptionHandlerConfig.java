package com.atguigu.exceptionhandler;


import com.atguigu.common_utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
//写到日志文件中的配置注解
@Slf4j
@ControllerAdvice
//配置异常处理类 异常统一处理
public class ExceptionHandlerConfig {
    //指定出现了什么异常会执行这个方法
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public R error(Exception e){
        //写到日志文件中的具体信息
        log.error("异常信息统一处理",e);
        return R.error().message("异常信息统一处理");
    }

    //自定义异常
    @ExceptionHandler(DIYException.class)
    @ResponseBody
    public R error(DIYException e){
        log.error("DIY异常信息统一处理",e);
        return R.error().code(e.getCodeDIY()).message(e.getMsgDIY());
    }

}
