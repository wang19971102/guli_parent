package com.atguigu.oss.services;

import com.atguigu.common_utils.R;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface OssServices {
 String uploadFileAvatar(MultipartFile file) throws IOException;
}
