package com.atguigu.oss.controller;

import com.atguigu.common_utils.R;
import com.atguigu.oss.services.OssServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("eduoss/fileoss")
@CrossOrigin
public class OssController {
    @Autowired
    private OssServices ossServices;
    @PostMapping
    public R uploadOssFile(MultipartFile file){
        String url = null;
        try {
            url = ossServices.uploadFileAvatar(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.ok().data("url",url);
    }
}
