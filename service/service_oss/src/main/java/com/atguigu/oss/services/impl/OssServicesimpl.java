package com.atguigu.oss.services.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.atguigu.common_utils.R;
import com.atguigu.oss.services.OssServices;
import com.atguigu.oss.utils.ConstantPropertiesUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

//上传文件到阿里云oss  返回oss文件路径
@Service
public class OssServicesimpl implements OssServices {
    @Override
    public String uploadFileAvatar(MultipartFile file) throws IOException {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = ConstantPropertiesUtils.ENDPOINT;
// 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录RAM控制台创建RAM账号。
        String accessKeyId = ConstantPropertiesUtils.KEYID;
        String accessKeySecret = ConstantPropertiesUtils.KEYSECRET;
        String bucketName = ConstantPropertiesUtils.BUCKETNAME;
// <yourObjectName>上传文件到OSS时需要指定包含文件后缀在内的完整路径，例如abc/efg/123.jpg。
        String objectName = "<yourObjectName>";

// 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        //获取上次文件输入流
        InputStream inputStream = file.getInputStream();
        //获取文件名称
        String filename = file.getOriginalFilename();
        //文件名称唯一
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        filename = uuid+filename;

        //给文件按日期建目录
        String dataPath = new DateTime().toString("yyyy/MM/dd");
        filename = dataPath+"/"+filename;
// 上传文件到指定的存储空间（bucketName）并将其保存为指定的文件名称（objectName）。
        ossClient.putObject(bucketName, filename, inputStream);
        //拼接文件路径
        String url ="https://"+bucketName+"."+endpoint+"/"+filename;
// 关闭OSSClient。
        ossClient.shutdown();
        return url;
    }
}
