package com.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

//excel字段
@Data
public class excelData {
    //Excel表头信息
    @ExcelProperty(value = "学生编号",index = 0)
    private Integer sno;
    @ExcelProperty(value = "学生姓名",index = 1)
    private String stuName;
}
