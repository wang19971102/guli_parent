package com.excel;

import com.alibaba.excel.EasyExcel;

import java.util.ArrayList;
import java.util.List;

public class TestWriteExcel {
    public static List<excelData> getList(){
        List<excelData> list =new ArrayList<>();
        for (int i = 0; i <10 ; i++) {
            excelData excelData =new excelData();
            excelData.setSno(i);
            excelData.setStuName("王"+i);
            list.add(excelData);
        }
        return list;
    }

    public static void main(String[] args) {
        //写入Excel数据
        //Excel文件路径和文件名
        String fileName="C:/wang/student.xlsx";

        //调用Excel写入工具类
        //传文件 +与 文件对应的Excel类                        sheet名
        EasyExcel.write(fileName,excelData.class).sheet("学生列表")
                .doWrite(getList());     //要写入的数据
        System.out.println(getList());
    }
}
