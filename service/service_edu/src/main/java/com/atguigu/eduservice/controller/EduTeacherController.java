package com.atguigu.eduservice.controller;


import com.atguigu.common_utils.R;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.entity.vo.VOEduTEacher;
import com.atguigu.eduservice.service.EduTeacherService;
import com.atguigu.exceptionhandler.DIYException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-08-30
 */
@Api(description = "讲师管理") //swagger测试页面的注释
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin  //解决跨域
public class EduTeacherController {
    @Autowired
    private EduTeacherService teacherService;
    //查全部
    @GetMapping("listAll")
    public R listAll(){
//        try {
//            int num=10/0;
//        }catch (Exception e){
//            throw new DIYException(999,"DIY自定义异常处理");
//        }

        return R.ok().data("list",teacherService.list(null));
    }
    @ApiOperation(value = "逻辑删除讲师")  //swagger测试页面的注释
    //根据id删除
    @DeleteMapping("{id}")
                             //swagger测试页面的注释                   required表示必填项    路径传递参数
    public R deleteById(@ApiParam(name = "id",value = "讲师ID",required = true) @PathVariable String id){
        EduTeacher eduTeacher = teacherService.getById(id);
        boolean f=false;
        if(eduTeacher!=null){
             f = teacherService.removeById(id);
        }else
            return R.error();
        return f ? R.ok() : R.error();
    }
    //分页
    @GetMapping("pageTeacher/{current}/{limit}")
    public R pageTeacher(@PathVariable long current,@PathVariable long limit){
        Page<EduTeacher> page =new Page<>(current,limit);
        teacherService.page(page, null);
        long total = page.getTotal();
        List<EduTeacher> records = page.getRecords(); //数据集合
        return R.ok().data("total",total).data("data",records);
    }
    //条件组合分页
    @PostMapping("pageTeacherCondition/{current}/{limit}")
    public R pageTeacherCondition(@PathVariable long current, @PathVariable long limit
                               //必须使用post请求  参数可以为空：required= false   @RequestBody主要用来接收前端传递给后端的json字符串中的数据的(请求体中的数据的)
                                  ,@RequestBody(required = false) VOEduTEacher voEduTEacher){
        //分页
        Page<EduTeacher> page = new Page<>(current,limit);
        QueryWrapper queryWrapper =new QueryWrapper();

        //前端传来的条件数据
        String name = voEduTEacher.getName();
        Integer level = voEduTEacher.getLevel();
        Date start = voEduTEacher.getStart();
        Date end = voEduTEacher.getEnd();


        //判断条件是否传值 与动态sql类似
        if(!StringUtils.isEmpty(name)){
            //模糊查询讲师名
            queryWrapper.like("name",name);
        }
        if (!StringUtils.isEmpty(level)){
            queryWrapper.eq("level",level);
        }
        //时间查询用的是数据库的字段名gmt_create  不是类的属性名gmtCreate 大坑
        if (!StringUtils.isEmpty(start)){
            queryWrapper.ge("gmt_create",start);
        }
        if(!StringUtils.isEmpty(end)){
            queryWrapper.le("gmt_create",end);
        }

        teacherService.page(page, queryWrapper);
        long total = page.getTotal();
        List<EduTeacher> records = page.getRecords();
        return R.ok().data("total",total).data("records",records);
    }

    //添加讲师
    @PostMapping("addTeacher")
    public R addTeacher(@RequestBody EduTeacher eduTeacher){
        boolean save = teacherService.save(eduTeacher);
        return save ? R.ok() :R.error();
    }

    //根据id查询
    @GetMapping("selectByID/{id}")
    public R selectByID(@PathVariable String id){
        EduTeacher teacher = teacherService.getById(id);
        if (teacher!=null){
            return R.ok().data("teacher",teacher);
        }
        return R.error();
    }
    //根据id查询
    @GetMapping("queryID/{id}")
    public EduTeacher queryID(@PathVariable String id){
        EduTeacher teacher = teacherService.getById(id);
        if (teacher!=null){
            return teacher;
        }
        return null;
    }

    //根据id修改
    @PostMapping("updateTeacher")
    public R updateTeacher(@RequestBody EduTeacher eduTeacher){
        boolean b = teacherService.updateById(eduTeacher);
        return b ? R.ok() :R.error();
    }
}

