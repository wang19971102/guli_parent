package com.atguigu.eduservice.service.impl;

import com.atguigu.eduservice.entity.EduChapter;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.entity.vo.VOEduChapter;
import com.atguigu.eduservice.entity.vo.VOEduVideo;
import com.atguigu.eduservice.mapper.EduChapterMapper;
import com.atguigu.eduservice.service.EduChapterService;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.exceptionhandler.DIYException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-06
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {
    @Autowired
    private EduChapterService eduChapterService;
    @Autowired
    private EduVideoService eduVideoService;

    @Override
    public List<VOEduChapter> selectChapterOrVideoByCourseId(String course_id) {
        //条件查询EduChapter
        QueryWrapper<EduChapter> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", course_id);
        queryWrapper.orderByAsc("gmt_create");
        List<EduChapter> eduChapters = eduChapterService.list(queryWrapper);

        //条件查询EduVideo
        QueryWrapper<EduVideo> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("course_id", course_id);
        List<EduVideo> eduVideos = eduVideoService.list(queryWrapper1);

        //组合封装VOEduChapter
        List<VOEduChapter> voEduChapters = new ArrayList<>();
        for (EduChapter eduChapter : eduChapters) {
            VOEduChapter voEduChapter = new VOEduChapter();
            BeanUtils.copyProperties(eduChapter, voEduChapter);
            voEduChapters.add(voEduChapter);

            String chapterId = eduChapter.getId();
            List<VOEduVideo> list = new ArrayList<>();
            for (EduVideo eduVideo : eduVideos) {
                if (chapterId.equals(eduVideo.getChapterId())) {
                    VOEduVideo voEduVideo = new VOEduVideo();
                    BeanUtils.copyProperties(eduVideo, voEduVideo);
                    list.add(voEduVideo);
                }
            }
            voEduChapter.setChildren(list);
        }
        return voEduChapters;
    }

    @Override
    public void deleteChapterOrVideo(String chapterId) {
        //删除章节  需要检查章节下的小节  有不能删除  没有可以删除
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("chapter_id", chapterId);
        int count = eduVideoService.count(queryWrapper);
        if (count > 0) {    //小节有数据不能删除
            throw new DIYException(500, "小节有数据不能删除");
        } else { //没有数据可以删除章节
            boolean b = eduChapterService.removeById(chapterId);
            if (!b) {
                throw new DIYException(500, "小节删除失败");
            }
        }

    }

    @Override
    public void removeChapterByCourseId(String courseId) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("course_id", courseId);
        baseMapper.delete(queryWrapper);
    }
}
