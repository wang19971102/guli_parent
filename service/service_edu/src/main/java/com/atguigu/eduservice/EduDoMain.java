package com.atguigu.eduservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
@EnableFeignClients    //feign服务调用
@EnableDiscoveryClient //nacos服务注册发现
@SpringBootApplication
//启动时根据包扫描 可以扫描其他项目中的包  只要符合规则
@ComponentScan(basePackages = {"com.atguigu"})
@EnableCaching
public class EduDoMain {
    public static void main(String[] args) {
        SpringApplication.run(EduDoMain.class, args);
    }
}
