package com.atguigu.eduservice.service;

import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.vo.CourseInfoVo;
import com.atguigu.eduservice.entity.vo.CoursePublishVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-06
 */
public interface EduCourseService extends IService<EduCourse> {
    String insertEduCourse(CourseInfoVo courseInfoVo);

    CourseInfoVo queryCourseInfo(String id);

    CoursePublishVo queryCoursePublishVo(String id);

    void updateCourseInfo(CourseInfoVo courseInfoVo);

    void removeCourse(String courseId);

    EduCourse selectById(String id);
}
