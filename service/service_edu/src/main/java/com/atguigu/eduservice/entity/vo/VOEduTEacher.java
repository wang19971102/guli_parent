package com.atguigu.eduservice.entity.vo;

import lombok.Data;

import java.util.Date;

@Data
public class VOEduTEacher {
    private String name;
    private Integer level;
    private Date start;
    private Date end;
}
