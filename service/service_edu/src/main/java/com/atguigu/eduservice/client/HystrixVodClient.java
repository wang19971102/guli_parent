package com.atguigu.eduservice.client;

import com.atguigu.common_utils.R;
import org.springframework.stereotype.Component;

@Component
public class HystrixVodClient implements VodClient {
    @Override
    public R removeAlyVideo(String id) {
        return R.error().message("删除视频出错了");
    }
}
