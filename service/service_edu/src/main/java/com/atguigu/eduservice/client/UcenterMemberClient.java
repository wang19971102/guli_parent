package com.atguigu.eduservice.client;

import com.atguigu.educenter.entity.UcenterMember;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Component
@FeignClient("service-ucenter")
public interface UcenterMemberClient {
    //查询token用户信息
    @GetMapping("/educenter/member/getInfo")
    UcenterMember getInfo(@RequestParam("request")HttpServletRequest request);
}
