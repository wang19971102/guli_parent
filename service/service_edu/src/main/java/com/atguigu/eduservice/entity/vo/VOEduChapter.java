package com.atguigu.eduservice.entity.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class VOEduChapter {
    private String id;
    private String title;
    private List<VOEduVideo> children =new ArrayList<>();
}
