package com.atguigu.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atguigu.eduservice.entity.EduSubject;
import com.atguigu.eduservice.entity.excel.SubjectData;
import com.atguigu.eduservice.entity.vo.VOEduOneSubject;
import com.atguigu.eduservice.entity.vo.VOEduTwoSubject;
import com.atguigu.eduservice.listener.SubjectExcelListener;
import com.atguigu.eduservice.mapper.EduSubjectMapper;
import com.atguigu.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-03
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override
    public void saveSubject(MultipartFile file,EduSubjectService eduSubjectService) {
        try {
            //读取Excel文件
            InputStream inputStream = file.getInputStream();
                                        //和excel数据对应的类      监听器 读取中执行的业务
            EasyExcel.read(inputStream, SubjectData.class, new SubjectExcelListener(eduSubjectService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<VOEduOneSubject> selectAllSubject() {
        //一级分类
        QueryWrapper<EduSubject> queryWrapper1 =new QueryWrapper<>();
        queryWrapper1.eq("parent_id","0");
        List<EduSubject> eduSubjects1 = this.list(queryWrapper1);

        //二级分类
        QueryWrapper<EduSubject> queryWrapper2 =new QueryWrapper<>();
        queryWrapper2.ne("parent_id","0");
        List<EduSubject> eduSubjects2 = this.list(queryWrapper2);

        //原类型转换一级分类
        List<VOEduOneSubject> oneVOEduSubject =new ArrayList<>();
        for (EduSubject e: eduSubjects1) {
            VOEduOneSubject voEduOneSubject =new VOEduOneSubject();
            voEduOneSubject.setId(e.getId());
            voEduOneSubject.setTitle(e.getTitle());
            oneVOEduSubject.add(voEduOneSubject);


        }

        //原类型转换二级分类
        for (VOEduOneSubject o: oneVOEduSubject) {
            List<VOEduTwoSubject> twoSubjects = new ArrayList<>();
            for (EduSubject e : eduSubjects2) {
                if(o.getId().equals(e.getParentId())) {
                    VOEduTwoSubject voEduTwoSubject = new VOEduTwoSubject();
                    voEduTwoSubject.setId(e.getId());
                    voEduTwoSubject.setTitle(e.getTitle());
                    twoSubjects.add(voEduTwoSubject);
                }
            }
            o.setChildren(twoSubjects);
        }
//        System.out.println(twoSubjects);
        //组合一二级分类
//        for (VOEduOneSubject o: oneVOEduSubject) {
//            List<VOEduTwoSubject> twoSubject2 = new ArrayList<>();
//            for (VOEduTwoSubject t: twoSubjects) {
//                if(o.getId().equals(t.getParent_id())){
//                    twoSubject2.add(t);
//                }
//
//            }
//            o.setChildren(twoSubject2);
//        }
        return oneVOEduSubject;
    }
}
