package com.atguigu.eduservice.controller.front;

import com.atguigu.common_utils.R;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.atguigu.eduservice.service.EduCourseService;
import com.atguigu.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("eduservice/indexfront")
public class IndexFrontController {
    @Autowired
    private EduCourseService eduCourseService;
    @Autowired
    private EduTeacherService eduTeacherService;
    @GetMapping("index")
    public R index(){
        //查询8条热门视频
        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("view_count");
        queryWrapper.last("limit 8");
        List<EduCourse> CourseList = eduCourseService.list(queryWrapper);

        //查询四名热门讲师
        QueryWrapper<EduTeacher> queryWrapper1 =new QueryWrapper();
        queryWrapper1.orderByDesc("id");
        queryWrapper1.last("limit 4");
        List<EduTeacher> teacherList = eduTeacherService.list(queryWrapper1);
        return R.ok().data("eduList",CourseList).data("teacherList",teacherList);
    }
}
