package com.atguigu.eduservice.controller;

import com.atguigu.common_utils.R;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/eduservice/user")
@CrossOrigin  //解决跨域
public class EduLoginController {

    @GetMapping("login")
    public R login(){
        return R.ok()
                .data("token","admin");
    }

    @GetMapping("info")
    public R info(){
        return R.ok()
                .data("roles","[admin]")
                .data("name","admin")
                .data("avatar","http://www.liulongbin.top:8000/resources/images/32.jpg");
    }
}
