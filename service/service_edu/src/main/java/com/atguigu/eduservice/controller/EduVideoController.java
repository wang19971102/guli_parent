package com.atguigu.eduservice.controller;


import com.atguigu.common_utils.R;
import com.atguigu.eduservice.client.VodClient;
import com.atguigu.eduservice.entity.EduVideo;
import com.atguigu.eduservice.entity.vo.VOEduVideo;
import com.atguigu.eduservice.service.EduVideoService;
import com.atguigu.exceptionhandler.DIYException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-09-06
 */
@RestController
@RequestMapping("/eduservice/video")
@CrossOrigin
public class EduVideoController {
    @Autowired
    private  EduVideoService eduVideoService;
    @Autowired
    private VodClient vodClient;
    @PostMapping("addVideo")
    public R addVideo(@RequestBody EduVideo eduVideo){
        boolean b = eduVideoService.save(eduVideo);
        return b ? R.ok() : R.error();
    }
    //删除小节删除阿里云视频
    @DeleteMapping("{id}")
    public R deleteVideo(@PathVariable String id){
        EduVideo eduVideo = eduVideoService.getById(id);
        String sourceId = eduVideo.getVideoSourceId();
        //删除小节删除阿里云视频
        if(!StringUtils.isEmpty(sourceId)){
            R r = vodClient.removeAlyVideo(sourceId);
            if(!r.getSuccess()){
                throw new DIYException(400,"哈哈删除失败");
            }
        }
        boolean b = eduVideoService.removeById(id);
        return b ? R.ok() :R.error();
    }
    @PostMapping("updateVideo")
    public R updateVideo(@RequestBody EduVideo eduVideo){
        boolean b = eduVideoService.updateById(eduVideo);
        return b ? R.ok() : R.error();
    }

    @GetMapping("getVideoInfo/{videoId}")
    public R getVideoInfo(@PathVariable String videoId){
        EduVideo video = eduVideoService.getById(videoId);
        return R.ok().data("video",video);
    }
}

