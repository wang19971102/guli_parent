package com.atguigu.eduservice.entity.vo;

import lombok.Data;

@Data
public class VOEduVideo {
    private String id;
    private String title;
}
