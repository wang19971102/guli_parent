package com.atguigu.eduservice.service.impl;

import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.eduservice.client.UcenterMemberClient;
import com.atguigu.eduservice.entity.EduComment;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.mapper.EduCommentMapper;
import com.atguigu.eduservice.service.EduCommentService;
import com.atguigu.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-23
 */
@Service
public class EduCommentServiceImpl extends ServiceImpl<EduCommentMapper, EduComment> implements EduCommentService {
    @Autowired
    private EduCourseService eduCourseService;
    @Autowired
    private UcenterMemberClient ucenterMemberClient;
    @Override
    public boolean insertComment(HttpServletRequest httpServletRequest, EduComment eduComment) {
        //获取评论信息 拼接评论内容
        UcenterMember ucenterMember = ucenterMemberClient.getInfo(httpServletRequest);
        EduCourse eduCourse = eduCourseService.selectById(eduComment.getCourseId());
        EduComment eduCommentAdd =new EduComment();
        //添加课程信息
        eduCommentAdd.setCourseId(eduCourse.getId());
        eduCommentAdd.setTeacherId(eduCourse.getTeacherId());
        //添加用户信息
        eduCommentAdd.setMemberId(ucenterMember.getId());
        eduCommentAdd.setNickname(ucenterMember.getNickname());
        eduCommentAdd.setAvatar(ucenterMember.getAvatar());
        //添加评论
        eduCommentAdd.setContent(eduComment.getContent());
        int insert = baseMapper.insert(eduCommentAdd);
        return insert == 0 ? false : true;
    }
}
