package com.atguigu.eduservice.entity.vo;

import lombok.Data;

@Data
public class VOEduTwoSubject {
    private String id;
    private String title;
}
