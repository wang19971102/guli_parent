package com.atguigu.eduservice.client;

import com.atguigu.common_utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "service-vod",fallback = HystrixVodClient.class)  //在edu里面注册vod服务
@Component
public interface VodClient {
    //根据视频id删除阿里云视频
    @DeleteMapping("/eduvod/video/removeAlyVideo/{id}")
     R removeAlyVideo(@PathVariable("id") String id);
}
