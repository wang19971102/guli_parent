package com.atguigu.educenter.mapper;

import com.atguigu.educenter.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author wgq
 * @since 2021-09-18
 */

public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

}
