package com.atguigu.educenter.controller;


import com.atguigu.common_utils.JwtUtils;
import com.atguigu.common_utils.R;
import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.educenter.entity.vo.RegisterVo;
import com.atguigu.educenter.service.UcenterMemberService;
import com.atguigu.ordervo.UcenterMemberOrder;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author wgq
 * @since 2021-09-18
 */
@RestController
@RequestMapping("/educenter/member")
@CrossOrigin
public class UcenterMemberController {
    @Autowired
    private UcenterMemberService ucenterMemberService;

    @PostMapping("login")
    public R login(@RequestBody UcenterMember ucenterMember, HttpServletResponse httpServletResponse){
        String token =ucenterMemberService.login(ucenterMember,httpServletResponse);
        return R.ok().data("token",token);
    }

    //注册
    @PostMapping("register")
    public R registerUser(@RequestBody RegisterVo registerVo) {
        ucenterMemberService.register(registerVo);
        return R.ok();
    }
    //查询token用户信息
    @GetMapping("getInfo")
    public UcenterMember getInfo(HttpServletRequest request){
        //用户id
        String id = JwtUtils.getMemberIdByJwtToken(request);
        //根据id查询用户
        UcenterMember ucenterMember = ucenterMemberService.selectByID(id);
        return ucenterMember;
    }
    //根据userid 返回评论的内容拼接
//    @GetMapping("getInfoUc/{id}")
//    public UcenterMember getInfoUc(@PathVariable String id){
//        UcenterMember ucenterMember = ucenterMemberService.getByid(id);
//        return ucenterMember;
//    }
}

