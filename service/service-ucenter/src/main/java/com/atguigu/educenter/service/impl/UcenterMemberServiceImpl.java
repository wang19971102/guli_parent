package com.atguigu.educenter.service.impl;

import com.atguigu.common_utils.JwtUtils;
import com.atguigu.common_utils.MD5;
import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.educenter.entity.vo.RegisterVo;
import com.atguigu.educenter.mapper.UcenterMemberMapper;
import com.atguigu.educenter.service.UcenterMemberService;
import com.atguigu.exceptionhandler.DIYException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author wgq
 * @since 2021-09-18
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public String login(UcenterMember ucenterMember,HttpServletResponse httpServletResponse) {
        String mobile = ucenterMember.getMobile();
        String password = ucenterMember.getPassword();

        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)){
            throw new DIYException(500,"手机号或密码为空！");
        }

        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("mobile",mobile);
        queryWrapper.eq("password", MD5.encrypt(password));
        UcenterMember member = baseMapper.selectOne(queryWrapper);

        if(member == null){
            throw new DIYException(500,"手机号未注册！");
        }

        if(member.getIsDisabled()){
            throw new DIYException(500,"手机被禁用！");
        }

        String jwtToken = JwtUtils.getJwtToken(member.getId(), member.getNickname());
        //token存head中
        httpServletResponse.addHeader("token",jwtToken);
        return jwtToken;
    }

    //注册的方法
    @Override
    public void register(RegisterVo registerVo) {
        //获取注册的数据
        String code = registerVo.getCode(); //验证码
        String mobile = registerVo.getMobile(); //手机号
        String nickname = registerVo.getNickname(); //昵称
        String password = registerVo.getPassword(); //密码

        //非空判断
        if(StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)
                || StringUtils.isEmpty(code) || StringUtils.isEmpty(nickname)) {
            throw new DIYException(20001,"注册失败");
        }
        //判断验证码
        //获取redis验证码
        String redisCode = (String) redisTemplate.opsForValue().get(mobile);
        if(!code.equals(redisCode)) {
            throw new DIYException(20001,"注册失败");
        }

        //判断手机号是否重复，表里面存在相同手机号不进行添加
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        Integer count = baseMapper.selectCount(wrapper);
        if(count > 0) {
            throw new DIYException(20001,"注册失败");
        }

        //数据添加数据库中
        UcenterMember member = new UcenterMember();
        member.setMobile(mobile);
        member.setNickname(nickname);
        member.setPassword(MD5.encrypt(password));//密码需要加密的
        member.setIsDisabled(false);//用户不禁用
        member.setAvatar("http://thirdwx.qlogo.cn/mmopen/vi_32/DYAIOgq83eoj0hHXhgJNOTSOFsS4uZs8x1ConecaVOB8eIl115xmJZcT4oCicvia7wMEufibKtTLqiaJeanU2Lpg3w/132");
        baseMapper.insert(member);
    }

    @Override
    public UcenterMember selectByID(String id) {
        QueryWrapper<UcenterMember> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id",id);
        UcenterMember ucenterMember = baseMapper.selectOne(queryWrapper);
        return ucenterMember;
    }
//
//    @Override
//    public UcenterMember getByid(String id) {
//        QueryWrapper<UcenterMember> queryWrapper =new QueryWrapper<>();
//        QueryWrapper<UcenterMember> wrapper = queryWrapper.eq("id", id);
//        UcenterMember member = baseMapper.selectOne(wrapper);
//        return member;
//    }
}
