package com.atguigu.educenter.service;

import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.educenter.entity.vo.RegisterVo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author wgq
 * @since 2021-09-18
 */

public interface UcenterMemberService extends IService<UcenterMember> {

    String login(UcenterMember ucenterMember, HttpServletResponse httpServletResponse);

    void register(RegisterVo registerVo);

    UcenterMember selectByID(String id);
}
