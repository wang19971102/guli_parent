package com.atguigu.serviceacl.service;

import com.atguigu.serviceacl.entity.AclUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
public interface AclUserRoleService extends IService<AclUserRole> {

}
