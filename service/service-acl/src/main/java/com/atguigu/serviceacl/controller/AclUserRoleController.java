package com.atguigu.serviceacl.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
@RestController
@RequestMapping("/serviceacl/acl-user-role")
public class AclUserRoleController {

}

