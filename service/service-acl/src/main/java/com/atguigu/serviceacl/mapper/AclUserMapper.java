package com.atguigu.serviceacl.mapper;

import com.atguigu.serviceacl.entity.AclUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
public interface AclUserMapper extends BaseMapper<AclUser> {

}
