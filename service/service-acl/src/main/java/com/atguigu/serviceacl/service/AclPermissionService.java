package com.atguigu.serviceacl.service;

import com.atguigu.serviceacl.entity.AclPermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.ArrayList;

/**
 * <p>
 * 权限 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
public interface AclPermissionService extends IService<AclPermission> {

    ArrayList<AclPermission> queryAllPermission();
    //递归删除 删除当前和下级菜单
    int deletePermission(String id);
}
