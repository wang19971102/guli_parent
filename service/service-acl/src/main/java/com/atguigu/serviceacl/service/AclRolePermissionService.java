package com.atguigu.serviceacl.service;

import com.atguigu.serviceacl.entity.AclRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
public interface AclRolePermissionService extends IService<AclRolePermission> {

}
