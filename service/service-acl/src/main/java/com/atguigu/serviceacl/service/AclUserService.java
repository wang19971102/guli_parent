package com.atguigu.serviceacl.service;

import com.atguigu.serviceacl.entity.AclUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
public interface AclUserService extends IService<AclUser> {

}
