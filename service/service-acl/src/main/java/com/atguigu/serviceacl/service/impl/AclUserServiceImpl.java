package com.atguigu.serviceacl.service.impl;

import com.atguigu.serviceacl.entity.AclUser;
import com.atguigu.serviceacl.mapper.AclUserMapper;
import com.atguigu.serviceacl.service.AclUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
@Service
public class AclUserServiceImpl extends ServiceImpl<AclUserMapper, AclUser> implements AclUserService {

}
