package com.atguigu.serviceacl.service.impl;

import com.atguigu.serviceacl.entity.AclPermission;
import com.atguigu.serviceacl.mapper.AclPermissionMapper;
import com.atguigu.serviceacl.service.AclPermissionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 权限 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
@Service
public class AclPermissionServiceImpl extends ServiceImpl<AclPermissionMapper, AclPermission> implements AclPermissionService {

    @Override
    public  ArrayList<AclPermission> queryAllPermission() {
        //查询的全部数据
        QueryWrapper<AclPermission> queryWrapper =new QueryWrapper<>();
        List<AclPermission> aclPermissions = baseMapper.selectList(queryWrapper);
        //封装用于返回的最终数据
        ArrayList<AclPermission> finalNodeList =new ArrayList<>();
        //根节点
        AclPermission rooNode = new AclPermission();
        aclPermissions.stream()
                .forEach(permissionNode ->{
                    if("0".equals(permissionNode.getPid())){
                        permissionNode.setLevel(1);
                        BeanUtils.copyProperties(permissionNode,rooNode);
                    }
        });
        //查询下级节点 封装数据
        selectChildNode(rooNode,aclPermissions);
        finalNodeList.add(rooNode);
        return finalNodeList;
    }
    //查询的全部数据递归
    void selectChildNode(AclPermission rooNode, List<AclPermission> aclPermissions) {
        ArrayList<AclPermission> childNodes = new ArrayList<>();
        aclPermissions.stream()
                .forEach(permissionChild -> {
                    if(rooNode.getId().equals(permissionChild.getPid())){
                        permissionChild.setLevel(rooNode.getLevel()+1);
                        childNodes.add(permissionChild);
                        if(rooNode.getChild()==null){
                            rooNode.setChild(new ArrayList<AclPermission>());
                        }
                        rooNode.getChild().add(permissionChild);
                        selectChildNode(permissionChild,aclPermissions);
                    }
                });
    }

    //递归删除 删除当前和下级菜单
    @Override
    public int deletePermission(String id) {
        ArrayList<String> listID = new ArrayList<>();
        selectID(id, listID);
        listID.add(id);
        int ids = baseMapper.deleteBatchIds(listID);
        return ids;
    }
    //递归删除
    void selectID(String id, ArrayList<String> listID) {
        QueryWrapper<AclPermission> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("pid",id);
        queryWrapper.select("id");
        List<AclPermission> aclPermissions = baseMapper.selectList(queryWrapper);
        aclPermissions.stream().forEach(items -> {
            listID.add(items.getId());
            selectID(items.getId(),listID);
        });
    }





    ArrayList list =new ArrayList();
    //计算斐波那契数列 第x位 递归
    static int first = 0;
    static int sed = 1;
    static int sum = 0;
    static int flag = 2; //标记
    //计算斐波那契
    void select() {
        list.add(first);
        list.add(sed);

        goToFibonacci(first,sed,sum,flag);
        System.out.println(list.toString());
    }

     void goToFibonacci(int first, int sed, int sum, int flag) {
       if(10>=flag) {
           sum = first + sed;
           first = sed;
           sed = sum;
           list.add(sum);
           flag += 1;
           goToFibonacci(first, sed, sum, flag);
       }
    }


    public static void main(String[] args) {
        AclPermissionServiceImpl acl = new AclPermissionServiceImpl();
        // acl.select();
//        System.out.println(acl.jiaHe());
    }
}
