package com.atguigu.serviceacl.mapper;

import com.atguigu.serviceacl.entity.AclPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
public interface AclPermissionMapper extends BaseMapper<AclPermission> {

}
