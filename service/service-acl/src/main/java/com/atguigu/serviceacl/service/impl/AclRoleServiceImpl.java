package com.atguigu.serviceacl.service.impl;

import com.atguigu.serviceacl.entity.AclRole;
import com.atguigu.serviceacl.mapper.AclRoleMapper;
import com.atguigu.serviceacl.service.AclRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
@Service
public class AclRoleServiceImpl extends ServiceImpl<AclRoleMapper, AclRole> implements AclRoleService {

}
