package com.atguigu.serviceacl.mapper;

import com.atguigu.serviceacl.entity.AclRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限 Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
public interface AclRolePermissionMapper extends BaseMapper<AclRolePermission> {

}
