package com.atguigu.serviceacl.mapper;

import com.atguigu.serviceacl.entity.AclUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
public interface AclUserRoleMapper extends BaseMapper<AclUserRole> {

}
