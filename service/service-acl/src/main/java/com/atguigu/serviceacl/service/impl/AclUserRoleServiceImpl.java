package com.atguigu.serviceacl.service.impl;

import com.atguigu.serviceacl.entity.AclUserRole;
import com.atguigu.serviceacl.mapper.AclUserRoleMapper;
import com.atguigu.serviceacl.service.AclUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
@Service
public class AclUserRoleServiceImpl extends ServiceImpl<AclUserRoleMapper, AclUserRole> implements AclUserRoleService {

}
