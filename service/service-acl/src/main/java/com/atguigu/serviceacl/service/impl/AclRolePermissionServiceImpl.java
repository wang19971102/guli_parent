package com.atguigu.serviceacl.service.impl;

import com.atguigu.serviceacl.entity.AclRolePermission;
import com.atguigu.serviceacl.mapper.AclRolePermissionMapper;
import com.atguigu.serviceacl.service.AclRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
@Service
public class AclRolePermissionServiceImpl extends ServiceImpl<AclRolePermissionMapper, AclRolePermission> implements AclRolePermissionService {

}
