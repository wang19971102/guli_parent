package com.atguigu.serviceacl.controller;


import com.atguigu.common_utils.R;
import com.atguigu.serviceacl.entity.AclPermission;
import com.atguigu.serviceacl.service.AclPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * <p>
 * 权限 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-09-28
 */
@SuppressWarnings("all")
@RestController
@RequestMapping("/serviceacl/acl-permission")
public class AclPermissionController {

    @Autowired
    private AclPermissionService aclPermissionService;

    @GetMapping("selectAllPermission")
    public ArrayList<AclPermission> selectAllPermission() {
        return aclPermissionService.queryAllPermission();
    }

    //递归删除 删除当前和下级菜单
    @DeleteMapping("{id}")
    public R delPermissionNode(@PathVariable String id) {
        int count = aclPermissionService.deletePermission(id);
        if (count != 0) {
            return R.ok();
        } else
            return R.error().message("删除失败!");
    }
}

