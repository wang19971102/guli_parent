package com.atguigu.eduorder.controller;


import com.atguigu.common_utils.R;
import com.atguigu.eduorder.entity.TOrder;
import com.atguigu.eduorder.service.TOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-09-24
 */
@RestController
@RequestMapping("/eduorder/t-order")
public class TOrderController {
    @Autowired
    private TOrderService tOrderService;
    @PostMapping("addOrder{id}")
    public TOrder addOrder(HttpServletRequest request, @PathVariable String courseId){
        TOrder tOrder = tOrderService.insertOrder(request,courseId);
        return tOrder;
    }
}

