package com.atguigu.eduorder.service.impl;

import com.atguigu.common_utils.R;
import com.atguigu.educenter.entity.UcenterMember;
import com.atguigu.eduorder.client.EduCourseClient;
import com.atguigu.eduorder.client.EduTeacherClient;
import com.atguigu.eduorder.client.UcenterMemberClient;
import com.atguigu.eduorder.entity.TOrder;
import com.atguigu.eduorder.mapper.TOrderMapper;
import com.atguigu.eduorder.service.TOrderService;
import com.atguigu.eduorder.utils.OrderNoUtil;
import com.atguigu.eduservice.entity.EduCourse;
import com.atguigu.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2021-09-24
 */
@Service
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements TOrderService {
    @Autowired
    private EduCourseClient eduCourseClient;
    @Autowired
    private UcenterMemberClient ucenterMemberClient;
    @Autowired
    private EduTeacherClient eduTeacherClient;
    @Override
    public TOrder insertOrder(HttpServletRequest request, String courseId) {
        //获取数据
        EduCourse eduCourse = eduCourseClient.selectById(courseId);
        UcenterMember ucenterMember = ucenterMemberClient.getInfo(request);
        //组装数据
        TOrder order = new TOrder();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(eduCourse.getId());
        order.setCourseTitle(eduCourse.getTitle());
        order.setCourseCover(eduCourse.getCover());
        EduTeacher eduTeacher = eduTeacherClient.queryID(eduCourse.getTeacherId());
        order.setTeacherName(eduTeacher.getName());
        order.setMemberId(ucenterMember.getId());
        order.setMobile(ucenterMember.getMobile());
        order.setNickname(ucenterMember.getNickname());
        order.setTotalFee(eduCourse.getPrice());
        order.setPayType(1);
        order.setStatus(1);
        return order;
    }
}
