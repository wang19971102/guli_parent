package com.atguigu.eduorder.service;

import com.atguigu.eduorder.entity.TOrder;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author testjava
 * @since 2021-09-24
 */
public interface TOrderService extends IService<TOrder> {

    TOrder insertOrder(HttpServletRequest request, String couseId);
}
