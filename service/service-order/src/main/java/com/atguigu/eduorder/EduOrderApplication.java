package com.atguigu.eduorder;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
@EnableFeignClients //开启feign注解  服务调用
@EnableDiscoveryClient //开启nacos注解  服务注册
@ComponentScan(basePackages = {"com.atguigu"})  //bean扫描
@MapperScan("com.atguigu.eduorder.mapper")     //mapper扫描
@SpringBootApplication
public class EduOrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(EduOrderApplication.class);
    }
}
