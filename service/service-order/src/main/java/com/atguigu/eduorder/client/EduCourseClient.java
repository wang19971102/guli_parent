package com.atguigu.eduorder.client;

import com.atguigu.eduservice.entity.EduCourse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient("service-edu")
public interface EduCourseClient {
    //对接评论的拼接功能 根据课程id查询单个的课程
    @GetMapping("/eduservice/course/selectById/{id}")
     EduCourse selectById(@PathVariable("id") String id);
}
