package com.atguigu.eduorder.client;

import com.atguigu.common_utils.R;
import com.atguigu.eduservice.entity.EduTeacher;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
@Component
@FeignClient("service-edu")
public interface EduTeacherClient {
    //根据id查询
    @GetMapping("/eduservice/teacher/queryID/{id}")
     EduTeacher queryID(@PathVariable("id") String id);
}
