package com.atguigu.ems.controller;


import com.atguigu.common_utils.R;
import com.atguigu.ems.entity.CrmBanner;
import com.atguigu.ems.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-09-15
 */
@RestController
@RequestMapping("/eduems/adminBanner")
@CrossOrigin
public class CrmBannerAdminController {
    @Autowired
    private CrmBannerService crmBannerService;
    //分页查询
    @GetMapping("pageBanner/{page}/{limit}")
    public R pageBanner(@PathVariable  long page, int limit){
        //分页对象
        Page<CrmBanner> bannerPage =new Page<>(page,limit);
                                        //查询条件
        crmBannerService.page(bannerPage,null);
        return R.ok().data("items",bannerPage.getRecords()).data("total",bannerPage.getTotal());
    }
    //添加首页banner
    @PostMapping("addBanner")
    public R addBanner(@RequestBody CrmBanner crmBanner){
        crmBannerService.save(crmBanner);
        return R.ok();
    }

    @PutMapping("updateBanner")
    public R updateBanner(@RequestBody CrmBanner crmBanner){
        crmBannerService.updateById(crmBanner);
        return R.ok();
    }
    @DeleteMapping("remode/{id}")
    public R removeBanner(@PathVariable String id){
        crmBannerService.removeById(id);
        return R.ok();
    }
    //查单个
    @GetMapping("getBannerID/{id}")
    public R getBannerID(@PathVariable String id){
        CrmBanner crmBanner = crmBannerService.getById(id);
        return R.ok().data("item",crmBanner);
    }
}

