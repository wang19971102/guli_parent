package com.atguigu.ems.controller;


import com.atguigu.common_utils.R;
import com.atguigu.ems.entity.CrmBanner;
import com.atguigu.ems.service.CrmBannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2021-09-15
 */
@RestController
@RequestMapping("/eduems/clientBanner")
@CrossOrigin
public class CrmBannerClientController {
    @Autowired
    private CrmBannerService crmBannerService;

    @GetMapping("getAllBanner")
    public R getAllBanner(){
      List<CrmBanner> CrmBannerList =  crmBannerService.selectAllBanner();
      return R.ok().data("list",CrmBannerList);
    }
}

