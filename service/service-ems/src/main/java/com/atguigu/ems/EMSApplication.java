package com.atguigu.ems;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
@MapperScan("com.atguigu.ems.mapper")  //Mapper扫描
@ComponentScan(basePackages = {"com.atguigu"})
@SpringBootApplication
public class EMSApplication {
    public static void main(String[] args) {
        SpringApplication.run(EMSApplication.class,args);
    }
}
