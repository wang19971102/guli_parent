package atguigu;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

public class TestRedis {
    @Autowired
    static RedisTemplate redisTemplate;         //操作对象
    @Autowired
    StringRedisTemplate stringRedisTemplate;    //操作字符串
    @Test
    public void test(){
        stringRedisTemplate.opsForValue().set("www","baidu");

    }
}